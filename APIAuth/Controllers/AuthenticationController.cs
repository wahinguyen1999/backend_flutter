﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIAuth.Business;
using AppGlobal.Filters;
using AppGlobal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private LoginBusiness LoginBusiness { get; set; }

        public AuthenticationController(LoginBusiness loginBusiness)
        {
            LoginBusiness = loginBusiness;
        }

        [HttpPost("Login")]
        public LoginResult<TokenAuthModel> Login([FromBody] LoginModel dataLogin)
        {
            return LoginBusiness.Login(dataLogin);
        }

        [HttpPost("Register")]
        public bool Register([FromBody] RegisterModel dataRegister)
        {
            return LoginBusiness.Register(dataRegister);
        }

        [HttpGet("AuthData")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public TokenAuthModel GetAuthData()
        {
            // var tokenID = Request.Headers["TokenID"].LastOrDefault();
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return LoginBusiness.GetAuthData(userID);
        }

        [HttpPut("UpdateAuthData")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public bool UpdateAuthData([FromBody] UpdateAuthModel dataUpdate)
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return LoginBusiness.UpdateAuthData(userID, dataUpdate);
        }
    }
}
