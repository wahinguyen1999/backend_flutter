﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIAuth.Business;
using AppGlobal.DBContext;
using AppGlobal.Extensions;
using AppGlobal.Filters;
using AppGlobal.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace APIAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 1. Lưu lại configuration values
            services.Configure<IConfiguration>(Configuration);

            // 2. Biến thành API keys
            services.ConfigureAPIServices(Configuration);

            // 3. Sử dụng DBContext chung cho 1 source => Dùng cái này
            services.AddDbContext<FlutterContext>(opt => opt.UseSqlServer(Configuration["FlutterStoreConnectionString"]));

            // 3. Sử dụng công cụ Context Factory để tạo DBContext riêng => Dùng cái này
            //services.AddSingleton<StoreContextFactory>();

            // 4. Thêm Services (Libraries)
            // REQUIRED: AccessTokenService - 
            services.AddScoped<AccessTokenService>();

            // 5. Thêm Filters (Phụ thuộc Services)
            services.AddScoped<IsAuthenticated>();

            // 6. Thêm Business (Phụ thuộc Services + DBContext)
            services.AddScoped<LoginBusiness>();

            // 7. Thêm Controllers files (Phụ thuộc Business)
            services.AddControllers(options =>
            {
                //options.Filters.Add(typeof(IsUserAuthenticated));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureApiApp(Configuration);
        }
    }
}
