﻿using AppGlobal.DBContext;
using AppGlobal.Entities.FlutterStoreEntities;
using AppGlobal.Models;
using AppGlobal.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIAuth.Business
{
    public class LoginBusiness
    {
        private AccessTokenService AccessTokenService { get; set; }
        private int TokenExpireDates { get; set; } = 365;
        private FlutterContext FlutterContext { get; set; }

        public LoginBusiness(AccessTokenService accessTokenService, FlutterContext flutterContext)
        {
            AccessTokenService = accessTokenService;
            FlutterContext = flutterContext;
        }

        public LoginResult<TokenAuthModel> Login(LoginModel data)
        {
            var user = FlutterContext.Users
                .AsEnumerable()
                .Where(u =>
                    u.UserCode.ToLower() == data.UserName.ToLower() &&
                    u.Password.ToLower() == data.Password.ToLower())
                .FirstOrDefault();

            if (user == null)
            {
                throw new Exception("Invalid Login Credentials!");
            }
            var authModel = new TokenAuthModel()
            {
                UserID = user.UserID,
                FullName = user.FullName,
                UserName = user.UserCode,
                Password = user.Password,
            };

            var tokenID = AccessTokenService.EncodeAuthModel(authModel);

            return new LoginResult<TokenAuthModel>()
            {
                TokenID = tokenID,
                AuthData = authModel,
            };
        }

        public bool Register(RegisterModel data)
        {
            var user = new Users()
            {
                FullName = data.FullName,
                UserCode = data.UserName,
                Password = data.Password,
            };

            FlutterContext.Users.Add(user);
            FlutterContext.SaveChanges();

            return true;
        }

        public bool UpdateAuthData(int userID, UpdateAuthModel dataUpdate)
        {
            var user = FlutterContext.Users
                .AsEnumerable()
                .Where(us => us.UserID == userID)
                .FirstOrDefault();

            if (user == null)
            {
                throw new Exception("Invalid User Data");
            }

            user.FullName = dataUpdate.FullName;
            user.UserCode = dataUpdate.UserName;
            user.Password = dataUpdate.Password;

            FlutterContext.SaveChanges();
            return true;
        }

        public TokenAuthModel GetAuthData(int userID)
        {
            var user = FlutterContext.Users
                .AsEnumerable()
                .Where(us => us.UserID == userID)
                .Select(us => new TokenAuthModel()
                {
                    UserID = us.UserID,
                    FullName = us.FullName,
                    UserName = us.UserCode,
                    Password = us.Password,
                }).FirstOrDefault();

            //return AccessTokenService.DecodeAuthModel<TokenAuthModel>(tokenID);
            return user;
        }
    }
}
