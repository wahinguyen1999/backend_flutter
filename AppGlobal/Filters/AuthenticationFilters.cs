﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AppGlobal.Services;
using AppGlobal.Models;
using AppGlobal.Extensions;
using AppGlobal.Commons;

namespace AppGlobal.Filters
{
    public class IsAuthenticated : ActionFilterAttribute
    {
        public AccessTokenService AccessTokenService { get; set; }

        public IsAuthenticated(AccessTokenService accessTokenService)
        {
            AccessTokenService = accessTokenService;
        }

        public string GetAccessTokenFromHeaders(ActionExecutingContext context)
        {
            // Tìm token trong headers
            HttpContext httpContext = context.HttpContext;

            string authToken = httpContext.Request.Headers["Authorization"].LastOrDefault();

            // Nếu k có
            if (string.IsNullOrEmpty(authToken))
            {
                // Tìm token trong query string
                authToken = httpContext.Request.Query["TokenID"].LastOrDefault();
            }

            return authToken;
        }

        public void ParseAndMapAccessTokenIntoHeaders(ActionExecutingContext context)
        {
            HttpContext httpContext = context.HttpContext;
            
            // Tìm token trong headers
            string authToken = GetAccessTokenFromHeaders(context);
            httpContext.Request.Headers.Add("TokenID", authToken);

            // Parse giá trị token
            TokenAuthModel authenData = AccessTokenService.DecodeAuthModel<TokenAuthModel>(authToken);

            // Map ngược vào Headers
            foreach (var entry in authenData.ConvertToDictionary())
            {
                // do something with entry.Value or entry.Key
                httpContext.Request.Headers.Add(entry.Key, entry.Value);
            }
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // Tìm token trong headers
            HttpContext httpContext = context.HttpContext;
            try
            {
                string authToken = GetAccessTokenFromHeaders(context);
                // Nếu k có
                if (string.IsNullOrEmpty(authToken))
                {
                    // Trả về 401 - Missing token
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Result = new JsonResult(new ApiErrorModel((int)ErrorCodes.AccessTokenIsMissing) { 
                        ExceptionMessage = "Token Is Missing!",
                    });
                    return;
                }
                // Kiểm tra token
                bool IsUserTokenValid = AccessTokenService.ValidateAccessToken(authToken);

                // Không hợp lệ
                if (!IsUserTokenValid)
                {
                    // Trả về 401 - K hợp lệ
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Result = new JsonResult(new ApiErrorModel((int)ErrorCodes.CredentialsInvalid)
                    {
                        ExceptionMessage = "Token Is Not Valid Or Expired!",
                    });
                    return;
                }

                ParseAndMapAccessTokenIntoHeaders(context);
            }
            catch (Exception ex)
            {
                // Trả về 401 - Missing token
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Result = new JsonResult(new { message = ex.Message });
                return;
            }
        }
    }

    public class IsUserType : ActionFilterAttribute
    {
        private readonly int[] UserTypeIDs;

        public IsUserType(params int[] userTypeIDs)
        {
            UserTypeIDs = userTypeIDs;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            HttpContext httpContext = context.HttpContext;
            try
            {
                int.TryParse(httpContext.Request.Headers["UserTypeID"].LastOrDefault(), out int userTypeID);

                if (!UserTypeIDs.Contains(userTypeID))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    context.Result = new JsonResult(new ApiErrorModel((int)ErrorCodes.UserIsUnauthorized)
                    {
                        ExceptionMessage = "Unauthorized To Access This Data!",
                    });
                    return;
                }
            }
            catch (Exception ex)
            {
                // Trả về 401 - Missing token
                httpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                context.Result = new JsonResult(new { message = ex.Message });
                return;
            }
        }
    }
}
