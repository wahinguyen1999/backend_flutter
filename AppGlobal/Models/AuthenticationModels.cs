﻿using AppGlobal.Commons;
using AppGlobal.Constants;
using Microsoft.AspNetCore.Diagnostics;
using System;
using System.Collections.Generic;

namespace AppGlobal.Models
{
    #region Authentication
    public class TokenAuthModel
    {
        public int UserID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class RegisterModel
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class UpdateAuthModel
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class ApiErrorModel
    {
        public int ErrorCode { get; set; }
        public string ErrorName { get; set; }
        public string ExceptionMessage { get; set; }

        public ApiErrorModel(int errorCode) {
            ErrorCode = errorCode;
            ErrorName = Enum.GetName(typeof(ErrorCodes), errorCode);
        }
    }
    public class LoginResult<TClass> where TClass : class
    {
        public string TokenID { get; set; }
        public TClass AuthData { get; set; }
    }
    #endregion
}
