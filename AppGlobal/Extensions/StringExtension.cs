﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;
using System.IO;
using System.Linq;
using Microsoft.IdentityModel.Tokens;

namespace AppGlobal.Extensions
{
    public static class StringExtension
    {
        public static string Base64Encode(this string plainText)
        {
            Base64UrlEncoder.Encode(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64UrlEncode(this string plainText)
        {
            return Base64UrlEncoder.Encode(plainText);
        }

        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Base64UrlDecode(this string base64EncodedData)
        {
            return Base64UrlEncoder.Decode(base64EncodedData);
        }
    }
}
