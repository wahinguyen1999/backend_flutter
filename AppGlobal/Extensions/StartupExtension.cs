﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;

namespace AppGlobal.Extensions
{
    public static class StartupExtension
    {
        public static IServiceCollection ConfigureAPIServices(this IServiceCollection Services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            Services.AddCors(options => options.AddPolicy("AllowAll", p => p
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowAnyOrigin()));

            Services
                .AddMvc(option => {
                    option.EnableEndpointRouting = false;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(opt => {
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Flutter Store API",
                    Description = "APIs my Software",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Nguyen Pham",
                        Email = string.Empty,
                        Url = new Uri("https://twitter.com/spboyer"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "GNU license",
                        Url = new Uri("https://example.com/license"),
                    }
                });
            });

            return Services;
        }

        public static IApplicationBuilder ConfigureApiApp(this IApplicationBuilder app, IConfiguration Configuration)
        {
            app.UseHttpsRedirection();

            app.UseCors("AllowAll");

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Flutter Store API");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync(string.Format("Welcome to API MicroService!"));
                });
            });

            return app;
        }
    }
}
