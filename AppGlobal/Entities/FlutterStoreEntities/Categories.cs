﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppGlobal.Entities.FlutterStoreEntities
{
    public partial class Categories
    {
        [Key]
        public int CategoryID { get; set; }
        [Required]
        [StringLength(50)]
        public string CategoryName { get; set; }
        public string ImageURL { get; set; }
    }
}
