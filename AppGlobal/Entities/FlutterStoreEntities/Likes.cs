﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppGlobal.Entities.FlutterStoreEntities
{
    public partial class Likes
    {
        [Key]
        public int LikeID { get; set; }
        public int ProductID { get; set; }
        public int UserID { get; set; }
    }
}
