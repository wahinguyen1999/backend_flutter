﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppGlobal.Entities.FlutterStoreEntities
{
    public partial class Users
    {
        [Key]
        public int UserID { get; set; }
        [Required]
        [StringLength(50)]
        public string UserCode { get; set; }
        [Required]
        [StringLength(50)]
        public string Password { get; set; }
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }
    }
}
