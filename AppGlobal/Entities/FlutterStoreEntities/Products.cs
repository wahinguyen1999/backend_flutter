﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AppGlobal.Entities.FlutterStoreEntities
{
    public partial class Products
    {
        [Key]
        public int ProductID { get; set; }
        public int? CategoryID { get; set; }
        [Required]
        [StringLength(150)]
        public string ProductName { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,0)")]
        public decimal Price { get; set; }
        [StringLength(500)]
        public string Image { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public int Featured { get; set; }
    }
}
