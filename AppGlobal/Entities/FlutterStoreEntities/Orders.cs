﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AppGlobal.Entities.FlutterStoreEntities
{
    public partial class Orders
    {
        [Key]
        public int OrderID { get; set; }
        public int UserID { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,0)")]
        public decimal PriceToTal { get; set; }
    }
}
