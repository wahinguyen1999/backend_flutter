﻿using Microsoft.Extensions.Configuration;
using AppGlobal.Extensions;
using System;
using AppGlobal.Models;
using System.Collections.Generic;
using System.Linq;

namespace AppGlobal.Services
{
    public class AccessTokenService
    {
        private string EncriptionKey { get; set; }

        public AccessTokenService(IConfiguration Configuration)
        {
            EncriptionKey = Configuration["AccessTokenEncriptionKey"];
        }

        public bool ValidateAccessToken(string tokenID)
        {
            List<string> parts = tokenID.Split('.').ToList();

            string data = parts[0];
            string signature = parts[1];

            bool isValid = data == signature.Base64UrlDecode();
            if (!isValid)
            {
                return false;
            }

            //TokenAuthModel authData = DecodeAuthModel<TokenAuthModel>(tokenID);
            //if (authData.DateExpired < DateTime.Now)
            //{
            //    return false;
            //}
            return true;
        }

        public string EncodeAuthModel<TClass>(TClass authModel) where TClass : class
        {
            string data = authModel.Serialize<TClass>().Base64UrlEncode();
            
            string signature = data.Base64UrlEncode();
            
            string tokenID = String.Format("{0}.{1}", data, signature);

            return tokenID;
        }

        public TClass DecodeAuthModel<TClass>(string tokenID) where TClass : class
        {
            List<string> parts = tokenID.Split('.').ToList();

            string data = parts[0].Base64UrlDecode();

            return data.Deserialize<TClass>();
        }
    }
}
