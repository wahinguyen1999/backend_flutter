﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGlobal.Constants
{
    public enum StatusID
    {
        Deleted = -1,
        Active = 1,
    }
}
