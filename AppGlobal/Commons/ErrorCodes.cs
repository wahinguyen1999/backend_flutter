﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGlobal.Commons
{
    public enum ErrorCodes
    {
        CredentialsInvalid = 1,
        OopsSomethingHapped,
        AccessTokenIsInValid,
        AccessTokenIsMissing,
        UserIsUnauthenticated,
        UserIsUnauthorized,
    }
}
