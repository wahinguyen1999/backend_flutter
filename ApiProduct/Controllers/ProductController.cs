﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiProduct.Business;
using AppGlobal.Entities.FlutterStoreEntities;
using AppGlobal.Filters;
using AppGlobal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiProduct.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductBusiness ProductBusiness;

        public ProductController(ProductBusiness productBusiness)
        {
            ProductBusiness = productBusiness;
        }

        [HttpGet("Search")]
        //[ServiceFilter(typeof(IsAuthenticated))]
        public List<Products> Search()
        {
            return ProductBusiness.Search();
        }

        [HttpGet("SearchProductByCategoryID")]
        //[ServiceFilter(typeof(IsAuthenticated))]
        public List<Products> SearchProductByCategoryID(int categoryID)
        {
            return ProductBusiness.SearchProductByCategoryID(categoryID);
        }

        [HttpGet("SearchProductLikes")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public List<Products> SearchProductLikes()
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return ProductBusiness.SearchProductLiked(userID);
        }

        [HttpPost("LikeProduct")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public bool LikeProduct(LikeProductModel dataBody)
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return ProductBusiness.LikeProduct(userID, dataBody);
        }

        [HttpGet("CheckLiked/{productID}")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public bool CheckLiked(int productID)
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return ProductBusiness.CheckLiked(userID, productID);
        }

        [HttpDelete("DeleteLike/{productID}")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public bool DeleteLike(int productID)
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return ProductBusiness.DeleteLike(userID, productID);
        }

        [HttpPost("Order")]
        [ServiceFilter(typeof(IsAuthenticated))]
        public bool Order(OrderModel bodyData)
        {
            int.TryParse(Request.Headers["UserID"].LastOrDefault(), out int userID);

            return ProductBusiness.Order(userID, bodyData);
        }
    }
}
