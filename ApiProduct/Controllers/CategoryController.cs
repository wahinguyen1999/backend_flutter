﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiProduct.Business;
using AppGlobal.Entities.FlutterStoreEntities;
using AppGlobal.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiProduct.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryBusiness CategoryBusiness;

        public CategoryController(CategoryBusiness categoryBusiness)
        {
            CategoryBusiness = categoryBusiness;
        }

        [HttpGet("Search")]
        public List<Categories> Search()
        {
            return CategoryBusiness.Search();
        }
    }
}
