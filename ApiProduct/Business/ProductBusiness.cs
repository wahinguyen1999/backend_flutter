﻿using AppGlobal.DBContext;
using AppGlobal.Entities.FlutterStoreEntities;
using AppGlobal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiProduct.Business
{
    public class ProductBusiness
    {
        private FlutterContext FlutterContext { get; set; }

        public ProductBusiness(FlutterContext flutterContext)
        {
            FlutterContext = flutterContext;
        }

        public List<Products> Search()
        {
            var products = FlutterContext.Products
                .AsEnumerable()
                .ToList();

            return products;
        }

        public List<Products> SearchProductByCategoryID(int categoryID)
        {
            var products = FlutterContext.Products
                .AsEnumerable()
                .Where(product => product.CategoryID == categoryID)
                .ToList();

            return products;
        }

        public List<Products> SearchProductLiked(int userID)
        {
            var productLikes = FlutterContext.Likes
                .AsEnumerable()
                .Where(like => like.UserID == userID)
                .Select(like => like.ProductID)
                .ToList();

            var products = FlutterContext.Products
                .AsEnumerable()
                .Where(product => productLikes.Contains(product.ProductID))
                .ToList();

            return products;
        }

        public bool LikeProduct(int userID, LikeProductModel dataLike)
        {
            var listLikes = FlutterContext.Likes
                .AsEnumerable()
                .Where(like => like.UserID == userID
                    && like.ProductID == dataLike.ProductID)
                .Any();

            if(listLikes) {
                throw new Exception("Data is exist");
            }    

            var productLike = new Likes() {
                UserID = userID,
                ProductID = dataLike.ProductID,
            };

            FlutterContext.Likes.Add(productLike);
            FlutterContext.SaveChanges();

            return true;
        }

        public bool CheckLiked(int userID, int productID)
        {
            var productLike = FlutterContext.Likes
                .AsEnumerable()
                .Where(like => like.UserID == userID
                    && like.ProductID == productID)
                .Any();

            if(productLike)
                return true;

            return false;
        }

        public bool DeleteLike(int userID, int productID)
        {
            var productLike = FlutterContext.Likes
                .AsEnumerable()
                .Where(like => like.UserID == userID
                    && like.ProductID == productID)
                .FirstOrDefault();

            if (productLike == null)
                throw new Exception("Data is not exist");

            FlutterContext.Remove(productLike);
            FlutterContext.SaveChanges();

            return true;
        }

        public bool Order(int userID, OrderModel dataCreate)
        {
            var order = new Orders()
            {
                UserID = userID,
                PriceToTal = dataCreate.PriceTotal,
            };

            FlutterContext.Orders.Add(order);
            FlutterContext.SaveChanges();
            return true;
        }
    }
}
