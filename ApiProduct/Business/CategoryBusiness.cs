﻿using AppGlobal.DBContext;
using AppGlobal.Entities.FlutterStoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiProduct.Business
{
    public class CategoryBusiness
    {
        private FlutterContext FlutterContext { get; set; }

        public CategoryBusiness(FlutterContext flutterContext)
        {
            FlutterContext = flutterContext;
        }

        public List<Categories> Search()
        {
            var categories = FlutterContext.Categories
                .AsEnumerable()
                .ToList();

            return categories;
        }
    }
}
