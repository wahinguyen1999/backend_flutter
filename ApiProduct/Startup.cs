﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiProduct.Business;
using AppGlobal.DBContext;
using AppGlobal.Extensions;
using AppGlobal.Filters;
using AppGlobal.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ApiProduct
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IConfiguration>(Configuration);

            services.ConfigureAPIServices(Configuration);

            services.AddDbContext<FlutterContext>(opt => opt.UseSqlServer(Configuration["FlutterStoreConnectionString"]));

            services.AddScoped<AccessTokenService>();
            services.AddScoped<IsAuthenticated>();
            services.AddScoped<ProductBusiness>();
            services.AddScoped<CategoryBusiness>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureApiApp(Configuration);
        }
    }
}
