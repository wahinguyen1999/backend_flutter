# .NET Core API Starter
This project contains source code template to start new API product using .NET Core 3.0

## Features (AppGlobal)
- .NET Core 3.0 API
- [Entity Framework Core 3.0](https://gitlab.com/a2ds/samples/net-core-api-starter/-/tree/master/AppGlobal/DBContext)
- Authentication with [JsonWebToken (JWT)](https://gitlab.com/a2ds/samples/net-core-api-starter/-/blob/master/AppGlobal/Services/AccessTokenService.cs)
-  [Extensions](https://gitlab.com/a2ds/samples/net-core-api-starter/-/tree/master/AppGlobal/Extensions)
- Commonly used [Services](https://gitlab.com/a2ds/samples/net-core-api-starter/-/tree/master/AppGlobal/Services)

This API uses only `GET` and `POST` request to communicate and HTTP [response codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) to indenticate status and errors. All responses come in standard JSON. All requests must include a `content-type` of `application/json` and the body must be valid JSON.

## Response Codes 
### Response Codes
```
200: Success
400: Bad request
401: Unauthorized
404: Cannot be found
405: Method not allowed
422: Unprocessable Entity 
50X: Server Error
```
### Example Error Message
```json
http code 402
{
    "errorCode": 1,
    "errorName": "CredentialsInvalid",
    "exceptionMessage": "Token is not valid or expired!"
}
```

## 1. `/Api/Authentication/Login`
**You send:**  Your  login credentials.
**You get:** An `API-Token` with wich you can make further actions.

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json
Content-Length: xy

{
    "UserName": "a2ds",
    "Password": "123" 
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "data": "uid.eyJ1c2VySUQiOjEsInVzZXJUeXBlSUQiOjMsImRhdGVFeHBpcmVkIjoiMjAyMS0wOS0yN1QwOTozMzoxNS40ODI4NDQ4KzA3OjAwIn0.f81d5e571a9165cd2831965c99714b5d"
}
```
**Failed Response:**
```json
HTTP/1.1 401 Unauthorized
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "errorCode": 1,
    "errorName": "CredentialsInvalid",
    "exceptionMessage": "Token is not valid or expired!"
}
``` 